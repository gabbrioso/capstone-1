let navbar = document.querySelector(".navbar");
let navbarHover = document.querySelector(".navbar-hover");
let navbarNav = document.querySelector(".navbar-nav");
let navbarLight = document.querySelector(".navbar-light");
let navbarBrand = document.querySelector(".navbar-brand")
let navLinkActive = document.querySelector(".nav-link.active")
let navLink = document.querySelector(".nav-link")
let navItem = document.querySelector(".nav-item")
let navText = document.querySelector(".nav-text")
let navText2 = document.querySelector(".nav-text2")
let dropdownItem = document.querySelector(".dropdown-item")

function invertNavbar(){
		navbar.setAttribute('style', 'background-color:rgba(0, 0, 0, 0.5) !important');
		navbar.style.transitionDuration = "1.5s";
		navLinkActive.setAttribute('style', 'color: white !important');
	    navLink.setAttribute('style', 'color: white !important');
	    navText.setAttribute('style', 'color: white !important');
	    navText2.setAttribute('style', 'color: white!important');
	    navbarBrand.setAttribute('style', 'color: white !important');
	    navItem.setAttribute('style', 'color: #fff !important');
	    navbarNav.setAttribute('style', 'color: white !important');
}

function revertNavbar(){
		navbar.setAttribute('style', 'background-color:none !important');
		navbar.style.transitionDuration = "1.5s";
		navLinkActive.setAttribute('style', 'color: black !important');
	    navLink.setAttribute('style', 'color: black !important');
	    navText.setAttribute('style', 'color: black !important');
	    navText2.setAttribute('style', 'color: black !important');
	    navbarBrand.setAttribute('style', 'color: black !important');
	    navItem.setAttribute('style', 'color: black !important');
	    navbarNav.setAttribute('style', 'filter: black !important');
}

navbarHover.addEventListener("mouseover", invertNavbar)
navbarHover.addEventListener("mouseout", revertNavbar)